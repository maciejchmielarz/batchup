@echo off
rem set code page for proper encoding in new.txt
chcp 1250
rem dt - date when script was started
for /f "tokens=2 delims==" %%i in ('wmic os get localdatetime /value') do set datetime=%%i
set dt=%datetime:~0,8%
rem exepath - directory with scripts
set exepath=%~dp0
rem cwdpath - directory where script was started
set cwdpath=%cd%
rem dirpath - directory to be archived full path
set dirpath=%1
rem drive - drive of directory to be archived
set drive=%dirpath:~1,3%
rem dirname - directory to be archived name 
set dirname=%dirpath:~4,-1%
set dirname=%dirname: =_%
rem password - to protect archived files
set password=%2
rem bucket - name serving as cloud archive flag
set bucket=%3
rem logpath - path to log file
set logpath=%cwdpath%\_Backup_log_%dt%.txt
echo === Start... %dirpath% %bucket% %date% %time% >> %logpath%
echo === Creating directory listing... >> %logpath%
rem list all files in dirpath recursively, compare with previous listing and create three
rem files: listing of files to be archived this time, temporary new.txt with full current
rem listing and temporary diff.txt with list of file paths to be archived (7zip parameter)
python %exepath%diff.py "%dirpath%" "%dirname%" "%dt%" >> %logpath%
rem change to drive of directory to be archived
cd /d %drive%
echo === Creating 7zip archive... >> %logpath%
rem in directory where script was started create 7zip archive with name of directory and 
rem current date based on files listed in diff.txt and encrypting files and headers with 
rem AES-256 using password 
7z a -t7z %cwdpath%\"%dirname%"_%dt%.7z @%cwdpath%\diff.txt -scsWIN -p%password% -mhe >> %logpath%
rem change to drive and directory where script was started
cd /d %cwdpath%
echo === Uploading to cloud (if any files were archived and flag was set)... >> %logpath%
rem upload to cloud if any files were archived and cloud archive flag is set
set awscmd="aws s3 cp %dirname%_%dt%.7z s3://%bucket%/%dt%/ --profile archive --storage-class DEEP_ARCHIVE --no-progress"
for /F %%A in ("%cwdpath%\diff.txt") do if %%~zA neq 0 if defined bucket %awscmd:"=% >> %logpath%
echo === Cleaning up... >> %logpath%
rem replace previous listing with the new one
move /y new.txt "%dirname%".txt
rem delete temporary list of files to be archived
del diff.txt
echo === Finished! %dirpath% %bucket% %date% %time% >> %logpath%
echo ===================================================================== >> %logpath%
