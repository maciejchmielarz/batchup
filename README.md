# Overview

Scripts to create incremental backups, compressed and encrypted using 7zip.

# User's Manual

## First Backup

1. Clone the repository or download scripts.
2. Create a directory where you want to store backups.
3. Copy `run.bat` to that directory.
4. Edit `run.bat` and add lines with directories to be archived.
5. Run `run.bat`.

## Incremental Backup

1. Run `run.bat`.

## Created Files

1. `Example_directory_YYYYMMDD.7z` - incremental backup file of the example directory
2. `Example_directory_YYYYMMDD.txt` - list of files included in the backup file
3. `Example_directory.txt` - most recent complete list of files in the example directory
4. `_Backup_log_YYYYMMDD.txt` - log file with information about processing

## Important Notes

In order to run the tool, you need to have `Python`, `7zip`, and `AWS CLI` (optional) installed, and their binaries need to be in the system `PATH`.

Format of the line in `run.bat` with directory to be archived is following:  
`call "[arch.bat relative path]" "[archived directory absolute path]" "[password]" "[bucket]"`
...where `arch.bat` path should be relative to the directory where `run.bat` is.

Only root directories on a drive partition can be archived, e.g. `D:\Temp`.

`bucket` is optional and is used to upload selected backup files to AWS S3 Glacier bucket. You need `AWS CLI`, an S3 bucket, and access profile named `archive` with write permissions to this bucket to use this option.
