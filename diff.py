import sys

from pathlib import Path
from datetime import datetime

# Get path and name of the archived directory
dir_path = sys.argv[1]
dir_name = sys.argv[2]

# Get archive create date
arch_date = sys.argv[3]

# Create empty file with previous listing of archived directory if not exists
with open(dir_name + ".txt", "a") as f:
    pass

# Read previous listing of archived directory
with open(dir_name + ".txt") as f:
    old = f.readlines()

# Read current listing of archived directory
new = [
    f"{datetime.fromtimestamp(path.stat().st_mtime).replace(microsecond=0)} {str(path)}\n"
    for path in Path(dir_path).rglob("*")
    if path.is_file()
]

# Create listing of files to archive by evaluating new minus old
# Preserve order by using list comprehension instead of set operation
diff = [f for f in new if not f in old]

# Write current listing of archived directory to file
with open("new.txt", "w") as f:
    f.writelines(new)

# Write listing of files to be archived to file
with open(dir_name + "_" + arch_date + ".txt", "w") as f:
    f.writelines(diff)

# Write list of relative file paths to be archived to file
with open("diff.txt", "w") as f:
    f.writelines([line[23:] for line in diff])

# Create and print status report
print(
    f"Stats on files to be archived",
    f"Directory:\t{dir_path}",
    f"Date:\t\t{arch_date}",
    f"File count:\t{len(diff)}",
    sep="\n",
)
